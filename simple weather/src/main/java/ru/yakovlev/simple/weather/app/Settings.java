package ru.yakovlev.simple.weather.app;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * Created by ayakovlev on 14.02.14.
 */
public class Settings extends Activity {

    int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    int interval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        Spinner spinner = (Spinner) findViewById(R.id.updateinterval);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.updateinterval, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                interval = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                //
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onClick (View v){

        String[] i = getResources().getStringArray(R.array.updateinterval);
        SharedPreferences sp = getSharedPreferences(WidgetBase.WIDGET_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(WidgetBase.WIDGET_COUNTRY + widgetID, i[interval]);
        editor.commit();
        this.finish();
    }

}
