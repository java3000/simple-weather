package ru.yakovlev.simple.weather.app;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.RemoteViews;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ayakovlev on 14.02.14.
 */
public  abstract class WidgetBase extends AppWidgetProvider {

    public static String ACTION_EXTEND = "extend";
    public final static String WIDGET_PREF = "widget_pref";
    public final static String WIDGET_COUNTRY = "widget_country_";
    public final static String WIDGET_CITY = "widget_city_";
    public final static String WIDGET_CITYID = "widget_city_id_";
    public final static String WIDGET_UNIT = "widget_unit_";
    public final static String WIDGET_TEMPERATURE = "widget_temperature_";
    public final static String WIDGET_WINDSPEED = "widget_windspeed_";
    public final static String WIDGET_HUMID = "widget_humid_";
    public final static String WIDGET_SUNSET = "widget_sunset_";
    public final static String WIDGET_SUNRISE = "widget_sunset_";
    public final static String WIDGET_LONGITUDE = "widget_longitude_";
    public final static String WIDGET_LATITUDE = "widget_latitude_";
    public final static String WIDGET_DESCRIPTION = "widget_description_";
    public final static String WIDGET_PRESSURE = "widget_pressure_";
    public final static String WIDGET_TEMPMIN = "widget_temp_min_";
    public final static String WIDGET_TEMPMAX = "widget_temp_max_";
    public final static String WIDGET_WINDDEGREE = "widget_winddegree_";
    public final static String WIDGET_CLOUDPERCENTAGE = "widget_cloudpercentage_";
    public final static String WIDGET_WEATHERCODE = "widget_weathercode_";
    public final static String WIDGET_WEATHERICON = "widget_weathericon_";
    public final static String WIDGET_ID = "widget_id_";
    public final static String WIDGET_RAIN = "widget_rain_";
    public final static String WIDGET_SNOW = "widget_snow_";
    private static Context ctx;

    public WidgetBase () {
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);

        SharedPreferences.Editor editor = context.getSharedPreferences(WIDGET_PREF, MODE_PRIVATE).edit();
        for (int widgetID : appWidgetIds) {
            editor.remove(WIDGET_COUNTRY + widgetID);
            editor.remove(WIDGET_CITY + widgetID);
            editor.remove(WIDGET_CITYID + widgetID);
            editor.remove(WIDGET_LONGITUDE + widgetID);
            editor.remove(WIDGET_LATITUDE + widgetID);
            editor.remove(WIDGET_TEMPERATURE + widgetID);
            editor.remove(WIDGET_WINDSPEED + widgetID);
            editor.remove(WIDGET_HUMID + widgetID);
            editor.remove(WIDGET_SUNRISE + widgetID);
            editor.remove(WIDGET_SUNSET + widgetID);
            editor.remove(WIDGET_DESCRIPTION + widgetID);
            editor.remove(WIDGET_PRESSURE + widgetID);
            editor.remove(WIDGET_TEMPMIN + widgetID);
            editor.remove(WIDGET_TEMPMAX + widgetID);
            editor.remove(WIDGET_WINDDEGREE + widgetID);
            editor.remove(WIDGET_CLOUDPERCENTAGE + widgetID);
            editor.remove(WIDGET_WEATHERCODE + widgetID);
            editor.remove(WIDGET_WEATHERICON + widgetID);
            editor.remove(WIDGET_UNIT + widgetID);
            editor.remove(WIDGET_ID + widgetID);
            editor.remove(WIDGET_RAIN + widgetID);
            editor.remove(WIDGET_SNOW + widgetID);
            editor.commit();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        if (intent.getAction().equals(ACTION_EXTEND)) {

            Bundle extras = intent.getExtras();
            if (extras != null) {
                mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            }
            if (mAppWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
                context.startActivity(intent);
            }
        }else {
            super.onReceive(context, intent);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        ctx = context.getApplicationContext();

        SharedPreferences sp = context.getSharedPreferences(WIDGET_PREF, MODE_PRIVATE);
        for (int id : appWidgetIds) {
            try {
                Intent intent = new Intent(context, MainActivity.class);
                intent.setAction(ACTION_EXTEND);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
                Uri data = Uri.withAppendedPath(Uri.parse("ayacall" + "://widget/id/"),String.valueOf(id));
                intent.setData(data);
                PendingIntent pIntent = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                RemoteViews views = UpdateWidget(context, sp, id);
                //TODO UpdateWidget(context, sp, id).setOnClickPendingIntent(R.id.pusharea , pIntent);
                views.setOnClickPendingIntent(R.id.pusharea , pIntent);
                appWidgetManager.updateAppWidget(id, views);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static RemoteViews UpdateWidget(Context context, SharedPreferences sp, int widgetID){

        String temperature = "";
        String windspeed = "";
        String humid = "";
        String temp_min = "";
        String temp_max = "";
        String rain = "";
        String snow = "";
        String description;
        String unit = sp.getString(WIDGET_UNIT + widgetID, null);
        String[] iddesc = ctx.getResources().getStringArray(R.array.weatherconditions);
        String[] idcode = ctx.getResources().getStringArray(R.array.weathercode);
        //TODO put it in each widget because different layouts!
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget1x4);

        try{
            temperature = sp.getString(WIDGET_TEMPERATURE + widgetID, null);
            windspeed = sp.getString(WIDGET_WINDSPEED + widgetID, null);
            humid = sp.getString(WIDGET_HUMID + widgetID, null);
            temp_min = sp.getString(WIDGET_TEMPMIN + widgetID, null);
            temp_max = sp.getString(WIDGET_TEMPMAX + widgetID, null);

        }catch(Exception e){
            e.printStackTrace();
        }

        views.setTextViewText(R.id.wid_temperature, temperature);
        views.setTextViewText(R.id.wid_windspeed, windspeed);
        views.setTextViewText(R.id.wid_humidity, humid);
        views.setTextViewText(R.id.wid_minmax, "(" + temp_min + "/" + temp_max + ")");
        views.setTextViewText(R.id.wid_cityname, sp.getString(WIDGET_CITY + widgetID, null));
        views.setTextViewText(R.id.wid_countyname, "[" + sp.getString(WIDGET_COUNTRY + widgetID, null) + "]");

        return views;
    }
}
