package ru.yakovlev.simple.weather.app;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;

/**
 * Created by ayakovlev on 14.02.14.
 */
public class DataService extends IntentService {


    public DataService() {
        super("DataService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //TODO need return actual data

        int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        Context ctx = getApplicationContext();
        SharedPreferences sp = ctx.getSharedPreferences(WidgetBase.WIDGET_PREF, MODE_PRIVATE);

        saveData(ctx, sp, widgetID);

    }

    public static JSONObject getJSON(String url) throws JSONException {

        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(builder.toString());
    }

    public static boolean isNetworkOnline(Context context) {
        boolean status = false;
        try {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED && netInfo.isConnected()) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public void saveData (Context ctx, SharedPreferences sp, int widgetID){

        String temperature;
        String windspeed;
        String humid;
        String temp_min;
        String temp_max;
        String rain = "";
        String snow = "";
        String description;
        String unit = sp.getString(WidgetBase.WIDGET_UNIT, null);
        JSONObject resp;

        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
        SharedPreferences.Editor editor = sp.edit();
        String[] iddesc = ctx.getResources().getStringArray(R.array.weatherconditions);
        String[] idcode = ctx.getResources().getStringArray(R.array.weathercode);

        try {

            //TODO try to reduce operations!
            resp = downloadData(ctx, sp, widgetID);

            if (resp.has("rain")){
                if (resp.getJSONObject("rain").has("3h")){
                    rain = resp.getJSONObject("rain").getString("3h") + " mm";
                }else{
                    rain = resp.getJSONObject("rain").getString("1h") + " mm";
                }
            }
            if (resp.has("snow")){
                snow = resp.getJSONObject("snow").getString("3h") + " mm";
            }

            if (unit.equals("metric")) {
                temperature = String.format("%.2f %s", Float.parseFloat(resp.getJSONObject("main").getString("temp"))," °C");
            }else{
                temperature = String.format("%.2f %s", Float.parseFloat(resp.getJSONObject("main").getString("temp"))," °F");
            }

            windspeed = resp.getJSONObject("wind").getString("speed") + " " + (ctx.getResources().getString(R.string.windsuffix));
            humid = resp.getJSONObject("main").getString("humidity")  + " %";
            String sunrise = sdf.format(Long.parseLong(resp.getJSONObject("sys").getString("sunrise")) * (long) 1000);
            String sunset = sdf.format(Long.parseLong(resp.getJSONObject("sys").getString("sunset")) * (long) 1000);
            String longitude = resp.getJSONObject("coord").getString("lon");
            String latitude = resp.getJSONObject("coord").getString("lat");
            String weathericon =resp.getJSONArray("weather").getJSONObject(0).getString("icon");
            String id = resp.getJSONArray("weather").getJSONObject(0).getString("id");
            String weathercode = resp.getJSONArray("weather").getJSONObject(0).getString("id");
            String pressure = resp.getJSONObject("main").getString("pressure") + " " + (ctx.getResources().getString(R.string.pascal));
            temp_min = String.format("%.2f", Float.parseFloat(resp.getJSONObject("main").getString("temp_min")));
            temp_max = String.format("%.2f", Float.parseFloat(resp.getJSONObject("main").getString("temp_max")));
            String winddegree = resp.getJSONObject("wind").getString("deg") + " °";
            String cloudpercentage = resp.getJSONObject("clouds").getString("all") + " %";

            editor.putString(WidgetBase.WIDGET_TEMPERATURE + widgetID, temperature);
            editor.putString(WidgetBase.WIDGET_WINDSPEED + widgetID, windspeed);
            editor.putString(WidgetBase.WIDGET_HUMID + widgetID, humid);
            editor.putString(WidgetBase.WIDGET_SUNRISE + widgetID, sunrise);
            editor.putString(WidgetBase.WIDGET_SUNSET + widgetID, sunset);
            editor.putString(WidgetBase.WIDGET_LONGITUDE + widgetID, longitude);
            editor.putString(WidgetBase.WIDGET_LATITUDE + widgetID, latitude);
            editor.putString(WidgetBase.WIDGET_PRESSURE + widgetID, pressure);
            editor.putString(WidgetBase.WIDGET_TEMPMIN + widgetID, temp_min);
            editor.putString(WidgetBase.WIDGET_TEMPMAX + widgetID, temp_max);
            editor.putString(WidgetBase.WIDGET_WINDDEGREE + widgetID, winddegree);
            editor.putString(WidgetBase.WIDGET_CLOUDPERCENTAGE + widgetID, cloudpercentage);
            editor.putString(WidgetBase.WIDGET_WEATHERCODE + widgetID, weathercode);
            editor.putString(WidgetBase.WIDGET_WEATHERICON + widgetID, "w" + weathericon);
            editor.putString(WidgetBase.WIDGET_SNOW + widgetID, snow);
            editor.putString(WidgetBase.WIDGET_RAIN + widgetID, rain);

            for (int i=0;i < idcode.length; i++){
                if(idcode[i].equals(id)){
                    description = iddesc[i];
                    editor.putString(WidgetBase.WIDGET_DESCRIPTION + widgetID, description);
                }
            }

            editor.putString(WidgetBase.WIDGET_ID + widgetID, id);
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String BuildUrl(SharedPreferences sp, int widgetID){
        String country = sp.getString(WidgetBase.WIDGET_COUNTRY + widgetID, null);
        String city = sp.getString(WidgetBase.WIDGET_CITY + widgetID, null);
        String cityID =  sp.getString(WidgetBase.WIDGET_CITYID + widgetID, null);
        String lon = sp.getString(WidgetBase.WIDGET_LONGITUDE + widgetID, null);
        String lat = sp.getString(WidgetBase.WIDGET_LATITUDE + widgetID, null);
        String unit = sp.getString(WidgetBase.WIDGET_UNIT, null);
        String url = "http://api.openweathermap.org/data/2.5/weather?";

        if (lat != null && lon != null){
            url = url + "lat=" + lat + "&lon=" + lon;
        }
        if (cityID!= null){
            url = url + "id=" + cityID;
        }else{
            url = url + "q=" + city + "," + country;
        }

        url = url + "&units=" + unit;
        url = url + "&APPID=17aefdee1b81db8a1e2d1b3ec2df3c5d";

        return url;
    }

    public JSONObject downloadData(Context ctx, SharedPreferences sp, int widgetID){
        JSONObject resp =null;
        String url = BuildUrl(sp, widgetID);
        if (isNetworkOnline(ctx)) {
            try {
                resp = getJSON(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return resp;
    }
}
